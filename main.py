import subprocess
from pathlib import Path
import os

for filepath in Path("scripts").rglob("refine_*.py"):
    print("PATH",filepath)
    subprocess.run("python " + str(filepath), shell=True, cwd = os.getcwd())
    print(filepath, "running")