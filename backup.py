
import sys
import os
import shutil
from datetime import date
import filecmp

if len(sys.argv) > 1:
    datasets = sys.argv[1:]
else: 
    datasets = os.listdir("datasets")
datasets = [x.upper() for x in datasets]

for dataset in datasets:
    original = f"datasets/{dataset}/{dataset.lower()}.csv"
    if os.path.exists(original):
        latest = sorted(os.listdir("datasets/"+dataset))[-1]
        latest = f"datasets/{dataset}/{latest}"
        # Copy if file changed
        if not filecmp.cmp(original, latest) or latest == original:
            target = f"datasets/{dataset}/{dataset.lower()}_{date.today()}.csv"
            shutil.copyfile(original, target)
            print(original, "=>", target)