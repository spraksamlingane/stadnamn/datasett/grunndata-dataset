# Generate templates for manually assigning suffixes to knr that have been used to identify more than one municipality
import pandas as pd
import os

def create_template(dataset, knr, kommune, start_year, end_year, encoding = "latin-1"):
    

    if not os.path.exists(f"updates/{dataset.lower()}/{dataset.upper()}_knr_suffixes.csv"):
        ref_source = "https://git.app.uib.no/spraksamlingane/stadnamn/autoritetsdata/administrative-enheter/-/raw/master/data/kommuner_over_tid.txt"
        knr_reference = pd.read_csv(ref_source, dtype = "string", sep="\t")

        data = pd.read_csv(f"datasets/{dataset.lower()}/{dataset.upper()}.csv", dtype="string", encoding = encoding,
                    keep_default_na=False, sep="\t")[[knr, kommune]].drop_duplicates()
        
        duplicate_knr = set(knr_reference[knr_reference["knr_suffix"] == "b"]["KNR"])
        data = data[data[knr].isin(duplicate_knr)]
        duplicates = knr_reference[knr_reference.KNR.isin(duplicate_knr)]


        suffix_map = duplicates[(duplicates.Opprettet <= start_year) & (duplicates.Opphør >= end_year)][["KNR", "knr_suffix"]].groupby("KNR").max().to_dict()["knr_suffix"]

        def get_suffix(x):
            if x in suffix_map:
                return  suffix_map[x]
            else:
                dataset_knames = data.loc[data[knr] == x, kommune].drop_duplicates()
                if len(dataset_knames) == 1:
                    kname = dataset_knames.item()
                    matches = duplicates.loc[duplicates.Kommune == kname]
                    if len(matches) == 1:
                        return matches.knr_suffix.item()
                return ""
            


        data["knr_suffix"] = data[knr].apply(lambda x: get_suffix(x))
        

        data = data.drop_duplicates()
        data = data.merge(duplicates[["KNR", "knr_suffix", "Kommune"]], left_on = [knr, "knr_suffix"], right_on = ["KNR", "knr_suffix"], how = "left", suffixes= ("_local", "_global"))
        
        # Move column to the right
        suffixes = data["knr_suffix"]
        data = data.drop("knr_suffix", 1)
        data["knr_suffix"] = suffixes
        
        data.to_csv(f"updates/{dataset.lower()}/{dataset.upper()}_knr_suffixes.csv", sep=";", index = False)

            
create_template("m1838", "KNR", "Prestegjeld", "1838", "1838")
create_template("m1886", "KNR", "Kommune", "1884", "1887")
create_template("rygh", "Knr", "Herred", "1898", "1912")
create_template("bsn", "KNR", "herred", "1940", "2022", encoding="utf-8")
create_template("mu1950", "KNR", "Kommune", "1940", "1955")






