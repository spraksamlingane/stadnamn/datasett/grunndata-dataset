import pandas as pd
from io import StringIO
import re
import csv
import helper_functions as hf

source = "archive/MU1950/20220303_MU1950.txt"
table = pd.read_csv(source, low_memory=False, dtype="string",
                    keep_default_na=False, sep=";", encoding = "latin-1")


# Missing gardnamn, temporary fix
table.loc[(table.Bruksnamn == ""), "Bruksnamn"] = "[uten navn]"
#table.loc[(table.Bruksnamn == ""), "Bruksnamn_SNID"] = ""

# Fix coordinate types
coordinate_type_map = {'Byggningscentroide, Språksamlingane': "Bygningscentroide",
                    'Bygningscentroide, Språksamlingane': "Bygningscentroide",
                    'Gardnummercentroide, Språksamlingane': "Gardnummercentroide",
                    'Hovedteigcentroide, Språksamlingane': "Kartverket Hovedteigcentroide"}
table['Koordinattype'] = table["Koordinattype"].apply(lambda x: coordinate_type_map.get(x, x))


# Replace "ö" with "ø"
table.Gardsnamn = table.Gardsnamn.apply(lambda x: x.translate(str.maketrans("Öö", "Øø")))
table.Bruksnamn = table.Bruksnamn.apply(lambda x: x.translate(str.maketrans("Öö", "Øø")))


table = hf.add_knr_suffixes("mu1950", table, "KNR")

# Fix SNID containing newline
table.loc[table.ID == "192396", "Gardsnamn_SNID"] = "SNID-NO-0005000100212"

# TESTS
assert len(set(table["KNR"].str.len())) == 1, "Leading zero missing in KNR"

table.to_csv("datasets/MU1950/mu1950.csv", sep="\t", index = False, encoding = 'latin-1')
