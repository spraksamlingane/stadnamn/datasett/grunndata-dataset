import pandas as pd
import re

COORDINATE_TYPE = ["Kartverket Lokalitetspunkt",
                   "Kartverket Adressepunkt", 
                   "Kartverket Bygningspunkt", 
                   "Bygningscentroide", 
                   "Lokalitetscentroide",
                   "Kartverket Hovedteigcentroide", 
                   "Gardnummercentroide", 
                   "Namnegardcentroide", 
                   "Kommunecentroide"]
COORDINATE_TYPE = {index: coordinate_type for coordinate_type, index in enumerate(COORDINATE_TYPE)}


snid_names = {}

def add_place_names(data, snid_col, name_col, coordinate_type_col, X_col = "X", Y_col = "Y", date_col=None, date = None):
    add_place_names.priority += 1
    priority = add_place_names.priority
    for row in data.itertuples():  
        snid = getattr(row, snid_col)


        if snid != "":       
            begin = date or getattr(row, date_col)

            # Extract timespan
            if "-" in begin:
                end = begin.split("-")[1]
                if len(end) == 2:
                    end = begin[:2] + end
                begin = begin[:4]
            else:
                end = begin

            agg_row = snid_names.get(snid, {})
            if not agg_row:
                snid_names[snid] = agg_row
                agg_row["priority"] = priority
                # Add timespan
                if len(begin) == 4 and len(end) == 4 and int(begin) <= int(end):
                    agg_row["begin"] = begin
                    agg_row["end"] = end
                else:
                    print("Year not matching:", begin, end)
            else:
                # Compare timespan to existing values
                if len(begin) == 4 and len(end) == 4 and int(begin) <= int(end):
                    if int(agg_row.get("begin", "9999")) > int(begin):
                        agg_row["begin"] = begin
                    if int(agg_row.get("end", "0")) < int(end):
                        agg_row["end"] = end

            
            # Add coordinate if not already added from a more prioritized dataset or place type
            if agg_row["priority"] <= priority:
                agg_row["priority"] = priority
                if "label" not in agg_row:
                    agg_row["label"] = getattr(row, name_col)
                
                if getattr(row, coordinate_type_col):
                    # Replace existing coordinates if existing coordinates are more coarse
                    if "geo" not in agg_row or COORDINATE_TYPE[getattr(row, coordinate_type_col)] < agg_row["geo"]["precision"]:
                        agg_row["geo"] = {"precision": COORDINATE_TYPE[getattr(row, coordinate_type_col)], "coord": {(getattr(row, X_col), getattr(row, Y_col))}}
                    # Append coordinates if existing coordinates have the same level of precision
                    elif COORDINATE_TYPE[getattr(row, coordinate_type_col)]  == agg_row["geo"]["precision"]:
                        agg_row["geo"]["coord"].add((getattr(row, X_col), getattr(row, Y_col)))

add_place_names.priority = 0



# M1886
m1886 = pd.read_csv(f"C:/Users/has022/repos/stadnamn/grunndata-dataset/datasets/M1886/m1886.csv", low_memory=False, dtype="string",
                keep_default_na=False, sep="\t", encoding = "latin-1")

garder = m1886.loc[m1886.contains_gard == "True"]

add_place_names(garder, "Gardsnamn_SNID", "Gardsnamn", "Koordinattype", date = "1886")
print("M1886_Gard", len(snid_names))

garder2 = m1886.loc[~m1886.Gardsnamn_SNID.isin(snid_names)]

# TODO: add gard snids without dedicated rows?

add_place_names(m1886, "Bruksnamn_SNID", "Bruksnamn", "Koordinattype", date = "1886")
print("M1886_Bruk", len(snid_names))

#M1838
m1838 = pd.read_csv(f"C:/Users/has022/repos/stadnamn/grunndata-dataset/datasets/M1838/m1838.csv", low_memory=False, dtype="string",
                keep_default_na=False, sep="\t", encoding = "latin-1")

add_place_names(m1838, "SNID", "Stadnamn", "Koordinat_type", date = "1838")
print("M1838", len(snid_names))

#RYGH
rygh = pd.read_csv(f"C:/Users/has022/repos/stadnamn/grunndata-dataset/datasets/RYGH/rygh.csv", low_memory=False, dtype="string",
                keep_default_na=False, sep="\t", encoding = "latin-1")

for place_type in ["Bruk", "Gard", "Namnegard", "Forsv. Lokalitet", "Forsv. lokalitet", "Sokn", "Herad/Sokn", "Herad"]:
    filtered = rygh.loc[rygh.Lokalitetstype == place_type]
    add_place_names(rygh, "SNID", "Stadnamn", "koordinattype", date_col = "År")


print("RYGH", len(snid_names))


bsn =  pd.read_csv(f"C:/Users/has022/repos/stadnamn/grunndata-dataset/datasets/BSN/bsn.csv", low_memory=False, dtype="string",
                keep_default_na=False, sep="\t")


add_place_names(bsn, "SNID", "navn", "KoordinatType", date_col = "År")
print("BSN", len(snid_names))

import statistics
print("Exporting data")
for snid in snid_names:
    # Validate timespan
    if "begin" in snid_names[snid]:
        assert int(snid_names[snid]["begin"]) <= int(snid_names[snid]["end"]), f"{snid_names[snid]['begin']} {snid_names[snid]['end']}"
    
    # Assemble coordinates
    if "geo" in snid_names[snid]:
        x_coordinates = [float(n[0]) for n in snid_names[snid]["geo"]["coord"]]
        y_coordinates = [float(n[1]) for n in snid_names[snid]["geo"]["coord"]]
        x = statistics.mean(x_coordinates)
        y = statistics.mean(y_coordinates)
        snid_names[snid]["X"] = x
        snid_names[snid]["Y"] = y




snid_df = pd.DataFrame.from_dict(snid_names).T[["label", "X", "Y", "begin", "end"]].reset_index()
snid_df.rename(columns = {"index":"SNID"}, inplace=True)

snid_df.to_csv("datasets/NSM/nsm.csv", index=False, sep="\t")