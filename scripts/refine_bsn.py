
import pandas as pd
import xml.etree.ElementTree as ET
import uuid
import xmltodict
import helper_functions as hf

source = "archive/BSN/bsn.csv"
table = pd.read_csv(source, dtype="string",
                    keep_default_na=False, sep="\t")

table.drop_duplicates(inplace = True)
print(table.columns)


# Rename knr-column
table.rename(columns = {"?KNR":"KNR"}, inplace=True)

"""# TEMPORARILY remove bruk without bnr
table = table[~((table["BNR"]=="") & (table["type"]=="bruk"))]
# TEMPORARILY remove rows without GNR
table = table[table["GNR"]!=""]
# TEMPORARILY remove underbruk without BNR
table = table[~((table["GNR"]=="") & (table["type"]=="underbruk"))]"""

table.loc[(table["BNR"]=="") & (table["type"]=="bruk"), "type"] = "eiendom"
table.loc[(table["GNR"]=="") & (table["type"]=="gard"), "type"] = "eiendom"
table.loc[(table["BNR"]!="") & (table["type"]=="underbruk"), "type"] = "bruk"


coordinate_type_map = {'Byggningscentroide, Språksamlingane': "Bygningscentroide",
                    'Bygningscentroide, Språksamlingane': "Bygningscentroide",
                    'Gardnummercentroide, Språksamlingane': "Gardnummercentroide",
                    'Gardnummercentroide, Språsamlingane': 'Gardnummercentroide',
                    'Hovedteigcentroide, Språksamlingane': "Kartverket Hovedteigcentroide"}
table['KoordinatType'] = table["KoordinatType"].apply(lambda x: coordinate_type_map.get(x, x))


# -------------------------------
# Insert grunndata
# -------------------------------



with open("archive/BSN/BSN_original.xml", "rb") as source:
    orig =xmltodict.parse(source.read())["steder"]["stnavn"]


# Extract years using biletesti and idnr (the latter for entries without image)
year_from_id = {x["@idnr"]: x.get("oppskr", {}).get("år") for x in orig}
year_from_image = {x["@bilete"]: x.get("oppskr", {}).get("år", "") for x in orig}


table.loc[table.biletesti == "", "År"] = table.stnavn_idnr.apply(lambda x: year_from_id.get(x, ""))
table.loc[table.biletesti != "", "År"] = table.biletesti.apply(lambda x: year_from_image.get(x, ""))

"""
# Get types for entries without underbruk
childless = {x for x in orig if not x.get("undbruk")}
childless_set = {x.get("@bilete") for x in childless if x.get("@bilete")}

table.loc[table.biletesti.isin()]
"""



# -------------------------------
# Add manifest
# -------------------------------
have_manifest = set()
with open("updates/bsn/manifest.txt") as f:
    for x in f:
        if "BSN" in x:
            break
        if len(x) == 44:
            have_manifest.add(x[2:-6])


seed_namespace = "http://data.toponym.ub.uib.no/bustadnamn/manifest/"
table["manifest"] = table["stnavn_idnr"].apply(lambda idnr: uuid.uuid3(uuid.NAMESPACE_URL, seed_namespace+idnr))
table["manifest"] = table["manifest"].apply(lambda x: f"https://iiif.spraksamlingane.no/iiif/manifest/{x}.json" if str(x) in have_manifest else "")


# Concatenate cadastral identifier
table["admID"] = table["KNR"]
table["admID"] += table["GNR"].apply(lambda x: "-" + x if x else "")
table["admID"] += table["BNR"].apply(lambda x: "/" + x if x else "")


# Replace incorrect values
table.loc[table.herred == "Bjørdal", "herred"] = "Ørsta"

# add knr-suffixes
table = hf.add_knr_suffixes("bsn", table, "KNR")

# -------------------------------
# Tests
# -------------------------------

# Leading zeroes
assert len(set(table["KNR"].str.len())) == 1

table.to_csv("datasets/BSN/bsn.csv", sep="\t", index = False)
