
import pandas as pd
import logging
import helper_functions as hf
logger = logging.getLogger(__name__)
logger.addHandler(logging.FileHandler("update_m1886.log"))
logger.setLevel(logging.DEBUG)


source = "archive/M1886/m1886.csv"
table = pd.read_csv(source, dtype="string",
                    keep_default_na=False, sep="\t", encoding = "latin-1")


def update(old_table, filepath, column_map = None, encoding = None):

    table = pd.read_csv(filepath, dtype = "string", keep_default_na=False, encoding = "latin-1")
    #table = pd.read_excel(filepath, dtype = "string", keep_default_na=False, engine = "openpyxl")
    #if encoding is not None:
    #    table = table.applymap(lambda x: x.encode(encoding).decode("latin-1"))
    #table["digitalisert_tekst"] = table["digitalisert_tekst"].map(lambda x: x.encode("utf8").decode("latin1"))
    if column_map is not None:
        table.rename(column_map, axis = 1, inplace = True)

    logger.info(f"Processing {filepath}")
    logger.info(f"Number of rows: {len(table)}")


    overlap = len(set(table["ID"]).intersection(set(old_table["ID"])))
    logger.info(f"Overlapping rows: {overlap}")

    table = pd.concat([old_table,table])
    no_duplicates = ~table.ID.duplicated(keep = 'last')

    table = table[no_duplicates].sort_index()

    for x in set(table.columns):
        if x not in old_table.columns:
            logger.warning(f"Column '{x}' in {filepath} not found in original dataset")
            table.drop(x, axis = 1, inplace = True)

    for x in set(old_table.columns):
        if x not in table.columns:
            logger.warning(f"Column '{x}' not found in {filepath}")

    return table


table = update(table, "updates/m1886/rhd_1886.csv")


# Merknader
table["Merknader"] = ""

# Replace content
table.rename(columns = {'Lenke til digital matrikkel':'Lenke_til_digital_matrikkel'}, inplace = True)

table.loc[table["ID"] == "24412", "Merknader"] = "Består av to hundre parseller, bruksnummer: 0301-75/29 til 0301-75/228"
table.replace('https://seeiendom.kartverket.no/eiendom/0301/75/29 til 228/0/0', 'https://seeiendom.kartverket.no/eiendom/0301/75/29', inplace = True)


# Strip hash symbol from links
stripped_links = table['Lenke_til_digital_matrikkel'].apply(lambda x: x.strip('#'))
table['Lenke_til_digital_matrikkel'] = stripped_links

# Replace incorrect labels
hf.map_from_other_column(table = table,
                         update_file = "updates/m1886/m1886_KNR_GNR_SNID_avvik_labels_fikset.csv",
                         columns = ["Gardsnamn"],
                         other_column = "Gardsnamn_SNID")

# Add missing names of municipalities & regions
hf.map_from_other_column(table = table,
                         update_file = "updates/m1886/m1886_manglende_kommunenavn.csv",
                         columns = ["Kommune", "Fylke"],
                         other_column = "KNR",
                         encoding = "utf-8")

# Fix coordinate types
coordinate_type_map = {'Byggningscentroide, Språksamlingane': "Bygningscentroide",
                    'Bygningscentroide, Språksamlingane': "Bygningscentroide",
                    'Gardnummercentroide, Språksamlingane': "Gardnummercentroide",
                    'Hovedteigcentroide, Språksamlingane': "Kartverket Hovedteigcentroide"}
table['Koordinattype'] = table["Koordinattype"].apply(lambda x: coordinate_type_map.get(x, x))

# Concatenate cadastral identifier
table["admID_gard"] = table["KNR"]
table["admID_bruk"] = table["KNR"]

table["admID_gard"] += table["GNR"].apply(lambda x: "-" + x if x else "")
table["admID_bruk"] += table["GNR"].apply(lambda x: "-" + x if x else "")

table["admID_bruk"] += table["BNR"].apply(lambda x: "/" + x if x else "")

# Update RHD links
table["Lenke_til_digital_matrikkel"] = table["Lenke_til_digital_matrikkel"].apply(lambda x: x.replace("www.", ""))

# Add knr suffix
knr_suffixes = pd.read_csv("updates/m1886/M1886_knr_suffixes.csv", sep=";", index_col="KNR", dtype="str")[["knr_suffix"]].to_dict()
print(knr_suffixes)
table["knr_suffix"] = table["KNR"].apply(lambda x: knr_suffixes.get(x, 'a'))

# remove duplicates
table.drop_duplicates(list(set(table.columns) - set(["ID"])), inplace=True)

#
garder = set(table.sort_values("BNR").drop_duplicates(["Gardsnamn_SNID"]).ID)
table["contains_gard"] = table.ID.apply(lambda x: x in garder)

# -------------------------------
# Tests
# -------------------------------

assert len(set(table["KNR"].str.len())) == 1, "Leading zero missing in KNR"

table.to_csv("datasets/M1886/m1886.csv", sep="\t", index = False, encoding = "latin-1")
