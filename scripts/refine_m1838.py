
import pandas as pd
import helper_functions as hf

gard = pd.read_csv("archive/M1838/20220221_M1838_Gard.txt", dtype="string",
                    keep_default_na=False, sep="\t", encoding = "latin-1")


bruk = pd.read_csv("archive/M1838/20220221_M1838_Bruk.txt", dtype="string",
                    keep_default_na=False, sep="\t", encoding = "latin-1")


# Remove duplicate Toen mnr 214
gard.drop(index = 10196, inplace= True)

# Merge gard and bruk
table = pd.concat([gard, bruk])

table.loc[table.Amt == "Lindaas", ["Prestegjeld", "Amt"]] = ["Lindaas", "Søndre Bergenhus"]
table.loc[table.Amt == "Sandsvers", ["Prestegjeld", "Amt"]] = ["Sandsvers", "Buskeruds"]

table['Lenke til digital matrikkel'] = table['Lenke til digital matrikkel'].apply(lambda x: x.strip('#'))
table['Lenke til skannet matrikkel'] = table['Lenke til skannet matrikkel'].apply(lambda x: x.strip('#'))


# Rename columns
table.columns = table.columns.str.replace(' ','_')

# Remove bruk without  name
table.loc[table.Stadnamn == "", ("Stadnamn", "SNID")] = "[uten navn]", ""

# add admID
table["admID_gard"] = table["KNR"]
table["admID_gard"] += table["MNR"].apply(lambda x: "-M" + x if not pd.isna(x) else "")
table["admID_bruk"] = table["admID_gard"]
table["admID_bruk"] += table["LNR"].apply(lambda x: "-L" + x if not pd.isna(x) else "")

table.Amt += " Amt"

# Add missing SNID
update = pd.read_csv("updates/m1838/20220221_M1838_SNIDmangler.txt",
                    dtype="string",
                    keep_default_na=False, sep="\t")
table.loc[(table.SNID == "") & (~(table.Stadnamn == '[uten navn]')), "SNID"] = tuple(update.SNID)

# Map coordinate types
coordinate_type_map = {'Bygningscentroide, Språksamlingane': "Bygningscentroide",
                    'Gardnummercentroide, Språsamlingane':  "Gardnummercentroide",
                    'Gardnummercentroide, Språksamlingane': "Gardnummercentroide",
                    'Hovedteigcentroide, Språksamlingane': "Kartverket Hovedteigcentroide"}
table['Koordinat_type'] = table["Koordinat_type"].apply(lambda x: coordinate_type_map.get(x, x))

table = hf.add_knr_suffixes("m1838", table, "KNR")

table.to_csv("datasets/M1838/m1838.csv", sep="\t", index = False, encoding = "latin-1")