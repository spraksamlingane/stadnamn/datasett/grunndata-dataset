from io import StringIO
import pandas as pd
import requests

# Fix separator error
source = "archive/K2019/k2019.csv"
with open(source, encoding="latin-1") as source_file:
    text = source_file.read()

fixed = StringIO(text.replace("\t lykt\t", " lykt\t"))
table = pd.read_csv(fixed, dtype="string",
                    keep_default_na=False, sep="\t")
table.to_csv("datasets/K2019/k2019.csv", sep="\t", index = False)
