import pandas as pd
def map_from_other_column(table, update_file, columns, other_column, encoding = "latin-1"):
    replacement = pd.read_csv(update_file, dtype = "string", keep_default_na=False, encoding = encoding)
    reset = table.set_index(other_column)
    reset.update(replacement.set_index(other_column))
    for column in columns:
        table[column] = list(reset[column])

def add_knr_suffixes(dataset, table, knr):
    knr_suffixes = pd.read_csv(f"updates/{dataset.lower()}/{dataset.upper()}_knr_suffixes.csv", sep=";", index_col="KNR", dtype="str")[["knr_suffix"]].to_dict()
    print(knr_suffixes)
    table["knr_suffix"] = table[knr].apply(lambda x: knr_suffixes.get(x, 'a'))
    return table