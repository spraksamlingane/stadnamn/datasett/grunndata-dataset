from io import StringIO
import pandas as pd
import requests

# Fix separator error
source = "archive/K2016/k2016.csv"
with open(source, encoding="latin-1") as source_file:
    text = source_file.read()

fixed = StringIO(text.replace("2121\t\t", "2121\t"))
table = pd.read_csv(fixed, dtype="string",
                    keep_default_na=False, sep="\t")
table.to_csv("datasets/K2016/k2016.csv", sep="\t", index = False)
