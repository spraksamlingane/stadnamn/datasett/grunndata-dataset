import pandas as pd
import xml.etree.ElementTree as ET
import numpy as np

source = "archive/HORD/Osteroy-2014-12-12.xml"

tree = ET.parse('C:/Users/has022/repos/stadnamn/grunndata-dataset/archive/HORD/Osteroy-2014-12-12.xml')
root = tree.getroot()

table = pd.DataFrame([{cell.tag: cell.text for cell in row} for row in root], dtype="string")

# Add mising county names
table["fylkesNamn"]= "Hordaland"

table["SNID-TMP"] = pd.Series(np.arange(len(table))).apply(lambda x: "SNID-TMP_" + str(x))


table.to_csv("datasets/HORD/hord.csv", sep="\t", index=False)