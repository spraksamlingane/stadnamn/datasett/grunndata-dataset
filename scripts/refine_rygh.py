
import pandas as pd
import requests
import xml.etree.ElementTree as ET
import helper_functions as hf

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.FileHandler("update_rygh.log"))
logger.setLevel(logging.DEBUG)


source = "archive/RYGH/rygh.csv"
table = pd.read_csv(source, dtype="string",
                    keep_default_na=False, sep="\t", encoding = "latin-1")

table.drop_duplicates(inplace = True)
table["unique"] = table["ID"] + "_" + table["stedsnummer"]

def update(old_table, filepath, column_map = None, encoding = None):
    table = pd.read_csv(filepath, dtype = "string", keep_default_na=False, encoding = "latin-1")
    #table = pd.read_excel(filepath, dtype = "string", keep_default_na=False, engine = "openpyxl")
    if encoding is not None:
        table = table.applymap(lambda x: x.encode(encoding).decode("latin-1"))
    #table["digitalisert_tekst"] = table["digitalisert_tekst"].map(lambda x: x.encode("utf8").decode("latin1"))
    if column_map is not None:
        table.rename(column_map, axis = 1, inplace = True)

    logger.info(f"Processing {filepath}")
    logger.info(f"Number of rows: {len(table)}")


    if not "stedsnummer" in table:
        table["stedsnummer"] = ""
    table["unique"] = table["ID"] + "_" + table["stedsnummer"]

    overlap = len(set(table["unique"]).intersection(set(old_table["unique"])))
    logger.info(f"Overlapping rows: {overlap}")

    table = pd.concat([old_table,table])
    no_duplicates = ~table.unique.duplicated(keep = 'last')

    table = table[no_duplicates].sort_index()

    for x in set(table.columns):
        if x not in old_table.columns:
            logger.warning(f"Column '{x}' in {filepath} not found in original dataset")
            table.drop(x, axis = 1, inplace = True)

    for x in set(old_table.columns):
        if x not in table.columns:
            logger.warning(f"Column '{x}' not found in {filepath}")

    return table


table = update(table, "updates/rygh/rygh_rettete feil.csv")
table = update(table, "updates/rygh/rygh_dublettfeil_2.csv")
table = update(table, "updates/rygh/dublettfeil_3.csv",
        column_map = {"dublettfeil_3_ID": "ID"})
table = update(table, "updates/rygh/dublettfeil_3a.csv")

# -------------------------------
# Replace content
# -------------------------------

table["Stadnamn"] = table["Stadnamn"].map(lambda x: "[uten navn]" if x == "UDEN NAVN" else x)

table.rename(columns = {'Lenke_til_ originalside': 'Lenke_til_originalside'}, inplace = True)

# Fix county label for Randøsund
table.loc[table["Knr"] == "1011", "Amt"] = "Lister og Mandal"

# Map source to year
year_map = {}
for item in set(table["Lenke_til_originalverk"]):
    response = requests.get(f"https://api.nb.no/catalog/v1/metadata/{item[24:]}/dublincore")
    root = ET.fromstring(response.text)
    date = root.find('{http://purl.org/dc/elements/1.1/}date').text
    year_map[item] = date

table["År"] = table["Lenke_til_originalverk"].apply(lambda x: year_map[x])

# Fix incorrect SNIDs
# Blekkerud
table.loc[table["SNID"] == "SNID-NO-0008600126767", "SNID"] = "SNID-NO-0008600126766"
# Mundheim
table.loc[table["SNID"] == "SNID-NO-0008600141205", "SNID"] = "SNID-NO-0008600141204"

# Fix coordinate types
coordinate_type_map = {'Bygningscentroide, Språksamlingane': "Bygningscentroide",
                    'Gardnummercentroide, Språksamlingane': "Gardnummercentroide",
                    'Gardnummercentroide, Språsamlingane': "Gardnummercentroide",
                    'Gardsnamncentroide, Språksamlingane': "Gardnummercentroide",
                    'Hovedteigcentroide, Språksamlingane': "Kartverket Hovedteigcentroide",
                    'Kommunecentroide, Språksamlingane': "Kommunecentroide",
                    'Lokalitetscentroide, Språksamlingane': "Lokalitetscentroide",
                    'Lokalitetspunkt, Statens kartverk': "Kartverket Lokalitetspunkt",
                    'Namnegardcentroide, Språksamlingane': "Namnegardcentroide"}
table['koordinattype'] = table.koordinattype.apply(lambda x: coordinate_type_map.get(x, x))


# Remove bnr from gard
table.loc[table["Lokalitetstype"] == "Gard", "Bnr"] = "" 


# Create cadastral identifier
table["admID"] = table["Knr"]
table["admID"] += table["Gnr"].apply(lambda x: "-" + x if x else "")
table["admID"] += table["Bnr"].apply(lambda x: "/" + x if x else "")


# -------------------------------
# Various updates, october 2021
# -------------------------------

# Remove namnegard with only bruk as children
alle_namnegard = set(table[(table.Lokalitetstype != "Bruk") & (table.Namnegard_SNID != "")]["Namnegard_SNID"])
bruk_namnegard = set(table[(table.Lokalitetstype == "Bruk") & (table.Namnegard_SNID != "")]["Namnegard_SNID"])
bare_bruk = set(table[(table.Lokalitetstype == "Namnegard") & (~(table.SNID.isin(alle_namnegard))) & (table.SNID.isin(bruk_namnegard))]["unique"])
table = table[~table.unique.isin(bare_bruk)]


"""updates = pd.read_csv("data/updates/rygh/Rygh_duplikater_garder_admID PG_sjekk2.csv", dtype="string",
                    keep_default_na=False, sep="\t")

# Generate missing unique ID
updates["unique"] = updates["ID"]+"_"+updates["stedsnummer"]

# Remove rows
remove = set(updates[updates.Slettes =="slet"]["unique"])
table = table[~table.unique.isin(remove)]

# Update rows
updates = updates[updates.Slettes != "slet"]
updates.drop(columns = ["Slettes", "kommentar", "id0"], inplace = True)
table = pd.concat([table, updates])
no_duplicates = ~table.unique.duplicated(keep = 'last')
table = table[no_duplicates].sort_index()"""


table = hf.add_knr_suffixes("rygh", table, "Knr")

# Fix whitespace MIDu
table.loc[table.MIDu == "1719022900 2", "MIDu"] = "1719022900-2"
table.loc[table.MIDu == "1719002200 5", "MIDu"] = "1719002200-5"

# -------------------------------
# Tests
# -------------------------------

# Leading zeroes
assert len(set(table["Fnr"].str.len())) == 1
assert len(set(table["Knr"].str.len())) == 1

table.to_csv("datasets/RYGH/rygh.csv", sep="\t", index = False, encoding = "latin-1")
