# grunndata-dataset

Grunnlagsfiler som brukes for å generere datasett til toponymi, lagt i git LFS.
Grunnlagsfiler som arkiveres kan legges til under archive/ med git add -f og versjonnr i filavnet.
I regelen skal ikke nye filer legges til i arkivet. Vi kan heller rekonstruere tidligere versjoner ved å benytte historikken i python-scriptene.

Datasettene i mappen "datasets" kan sikkerhetskopieres ved å kjøre backup.py med koden for ønsket datasett som argument. Hvis man kjører scriptet uten argument vil alle endrede datasett sikkerhetskopieres.

